import React, {Component} from 'react'
import {
    View,
    Text,
    ActivityIndicator,
    StyleSheet,
    Dimensions
} from 'react-native'

const {width, height} = Dimensions.get('window')

export default class Loading extends Component{
    render(){
        if (this.props.isLoading){
            return(
                <View style={styles.background}>
                    <View style={styles.loading}>
                        <ActivityIndicator size="large" color="#00ff00"/>
                        <Text style={styles.text}>Loading</Text>
                    </View>
                </View>
            )
        }
        return <View/>
    }
}

const styles = StyleSheet.create({
    background:{
        flex:1,
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: "#00000099", 
        width, 
        height, 
        position: 'absolute',
        bottom:0,
        elevation:8
       
    },
    loading:{
        width:'90%',
        height:height/10,
        backgroundColor:'white',
        justifyContent:"flex-start",
        flexDirection: "row",
        alignItems:'center',
        paddingLeft:10,
        borderRadius:10,
    },
    text:{
        marginLeft:15
    }
})