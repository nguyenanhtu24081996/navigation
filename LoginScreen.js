import React, {Component} from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Dimensions
}from 'react-native'
import Loading from './Loading';
const {width,height} = Dimensions.get('window')
export default class LoginScreen extends Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading:false
        }
    }
    _navigateToMyTab(){
        const {navigation} = this.props;
        this.setState({
            isLoading:true
        })
        setTimeout(function(){ 
           navigation.navigate('MyTab')
        }, 3000);
    }
    render(){
        const {navigation} = this.props
        return(
            <View style={styles.container}>
                <TouchableOpacity
                style={styles.button}
                onPress={() => this._navigateToMyTab()}>
                    <Text>LoginScreen</Text>
                </TouchableOpacity>
                <Loading
                    isLoading={this.state.isLoading}
                />
            </View>   
        )
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center', 
        alignItems: 'center',
        width, 
        height, 
    },
    button:{
        width:'90%',
        height:height/15,
        justifyContent:"center",
        alignItems:'center',
        backgroundColor:'yellow',
        borderRadius:10
    }
})