/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import 'react-native-gesture-handler';
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
const Stack = createStackNavigator();
import MyTab from './MyTab'
import LoginScreen from './LoginScreen'

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" backgroundColor='transparent' translucent={true} />
      <NavigationContainer>
      <Stack.Navigator headerMode={'none'}>
        <Stack.Screen name="LoginScreen" component={LoginScreen}/>
        <Stack.Screen name="MyTab" component={MyTab}/>
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

const styles = StyleSheet.create({
 
});

export default App;
