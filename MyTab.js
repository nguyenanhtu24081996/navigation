import React, {Component} from 'react'
import {
    View,
    Text
}from 'react-native'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'

const Tab = createBottomTabNavigator();
import HomeScreen from './HomeScreen'
import SettingsScreen from './SettingsScreen'
export default class MyTab extends Component{
    render(){
        return(
            <Tab.Navigator>
                <Tab.Screen name="Home" component={HomeScreen} />
                <Tab.Screen name="Settings" component={SettingsScreen} />
            </Tab.Navigator>
        )
    }
}